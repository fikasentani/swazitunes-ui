export interface UserProfile {
    id: string;
    firstName: string;
    lastName: string;
    imagePath: string;
    creator: string;
}

export interface UserProfileInfo {
    id: string;
    firstName: string;
    lastName: string;
    imagePath: string;
}