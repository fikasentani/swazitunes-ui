export interface Song {
    id: string;
    title: string;
    artist: string;
    songPath: string;
    creator: string;
}