export interface Tune {
    id: string;
    title: string;
    artist: string;
    imagePath: string;
    tunePath: string;
    creator: string;
}