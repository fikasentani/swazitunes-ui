import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AudioComponent } from './components/server-side/audio/audio/audio.component';
import { UpdateUserProfileComponent } from './components/user-profile/update-user-profile/update-user-profile.component';
import { HomeComponent } from './components/home/home.component';
import { AuthGuard } from './components/auth/auth.guard';
import { SongCreateComponent } from './components/songs/song-create/song-create.component';
import { SongListComponent } from './components/songs/song-list/song-list.component';
import { TuneCreateComponent } from './components/tunes/tune-create/tune-create.component';
import { TuneListComponent } from './components/tunes/tune-list/tune-list.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'user-profile/:userId',
    component: UserProfileComponent,
  },
  {
    path: 'update-user-profile',
    component: UpdateUserProfileComponent,
    // canActivate: [ AuthGuard ]
  },
  {
    path: 'audio',
    component: AudioComponent,
  },
  {
    path: 'update-user-profile/:userId',
    component: UpdateUserProfileComponent,
  },
  {
    path: 'lalela',
    component: SongListComponent,
  },
  {
    path: 'add',
    component: SongCreateComponent,
  },
  {
    path: 'condzisa/:songId',
    component: SongCreateComponent,
  },
  {
    path: 'upload',
    component: TuneCreateComponent,
  },
  {
    path: 'edit/:tuneId',
    component: TuneCreateComponent,
  },
  {
    path: 'listen',
    component: TuneListComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
