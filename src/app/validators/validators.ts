import { AbstractControl } from '@angular/forms';

export function PasswordValidator(
    control: AbstractControl
) : { [key: string]: boolean } | null {
    const password = control.get('rPassword');
    const confirmPassword = control.get('rConfirmPassword');

    return password && confirmPassword && password.value !== confirmPassword.value 
    ? { passwordMisMatch: true }
    : null; 
}