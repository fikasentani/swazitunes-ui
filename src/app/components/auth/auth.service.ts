import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthData } from './auth-data.model';
import { tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { BehaviorSubject, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { UserProfileInfo } from 'src/app/models/user-profile.model';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private isAuthenticated = false;
    newIsAuthenticated = new BehaviorSubject(false);
    private token: string;
    private tokenTimer: any;
    private userId: string;
    private email: string;
    private authStatusListener = new Subject<boolean>();
    private apiUrl = environment.baseApiUrl;
    showRegSuccessNotification = new BehaviorSubject(false);
    isLoading = new BehaviorSubject(false);
    loginError = new BehaviorSubject('');

    constructor(private http: HttpClient, private router: Router) {
    }

    getToken() {
        console.log('landza-->', this.token);
        return this.token = JSON.parse(localStorage.getItem('token'));
    }

    getIsAuth() {
        return this.isAuthenticated;
    }

    getUserId() {
        return this.userId;
    }

    getAuthStatusListener() {
        return this.authStatusListener.asObservable();
    }

    createUser(email: string, password: string) {
        const authData: AuthData = { email, password };
        return this.http.post(`${this.apiUrl}/api/auth/signup`, authData);
    }

    login(email: string, password: string) {
        this.isLoading.next(true);
        const authData: AuthData = { email, password };
        return this.http.post<{token: string; expiresIn: number, userId: string, email: string }>(
            `${this.apiUrl}/api/auth/login`, authData
        )
        .subscribe({
            next: response => {
                const token = response.token;
                this.token = token;
    
                if (token) {
                    const expiresInDuration = response.expiresIn;
                    this.setAuthTimer(expiresInDuration);
                    this.isAuthenticated = true;
                    this.userId = response.userId;
                    this.email = response.email;
                    this.authStatusListener.next(true);
                    const now = new Date();
                    const expirationDate = new Date(now.getTime() + expiresInDuration * 1000);
                    console.log(expirationDate);
                    this.saveAuthData(token, expirationDate, this.userId, this.email);
                    this.isLoading.next(false);
                    this.newIsAuthenticated.next(true);
                    this.router.navigate(['/']);
                }
            },
            error: (err) => {
                this.loginError.next('Login in details not Authorized!');
                console.log(err);
                this.isLoading.next(false);
            }
        }
        );
    }

    autoAuthUser() {
        const authInformation = this.getAuthData();
        if (!authInformation) {
            return;
        }
        const now = new Date();
        const expiresIn = authInformation.expirationDate.getTime() - now.getTime();
        if (expiresIn > 0) {
            this.token = authInformation.token;
            this.isAuthenticated = true;
            this.newIsAuthenticated.next(true);
            this.userId = authInformation.userId;
            this.setAuthTimer(expiresIn / 1000);
            this.authStatusListener.next(true);
        }
    }

    logout() {
        this.token = null;
        this.isAuthenticated = false;
        this.newIsAuthenticated.next(false);
        this.authStatusListener.next(false);
        this.userId = null;
        clearTimeout(this.tokenTimer);
        this.clearAuthData();
        this.router.navigateByUrl('/');
    }

    private setAuthTimer(duration: number) {
        console.log(`setting timer: ${duration}`);
        this.tokenTimer = setTimeout(() => {
            this.logout();
        }, duration * 1000);
    }

    private saveAuthData(token: string, expirationDate: Date, userId: string, email: string) {
        localStorage.setItem('token', JSON.stringify(token));
        localStorage.setItem('expiration', expirationDate.toISOString());
        localStorage.setItem('userId', userId);
        localStorage.setItem('email', email);
    }

    private clearAuthData() {
        localStorage.removeItem('token');
        localStorage.removeItem('expiration');
        localStorage.removeItem('userId');
    }

    getUserInfo(userId) {
        return this.http.get<{_id: string, firstName: string, lastName: string, imagePath: string}>(`${this.apiUrl}/api/auth/${userId}/profile`);
    }

    updateUserInfo(id: string, firstName: string, lastName: string, imagePath: string) {
        const userInfo: UserProfileInfo = {
            id,
            firstName,
            lastName,
            imagePath
        };
        this.http
        .patch(`${this.apiUrl}/api/auth/${id}/update-profile`, userInfo)
        .subscribe(response => {
            this.router.navigate(['/user-profile', this.userId]);
        });
    }

    private getAuthData() {
        const token = localStorage.getItem('token');
        const expirationDate = localStorage.getItem('expiration');
        const userId = localStorage.getItem('userId');
        if(!token || !expirationDate) {
            return;
        }
        return {
            token,
            expirationDate: new Date(expirationDate),
            userId
        };
    }
}
