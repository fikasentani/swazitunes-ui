import { HttpInterceptor, HttpRequest, HttpHandler } from '@angular/common/http';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private authService: AuthService) {}

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        console.log('bearseatbeats-->', req.url);
        console.log('bearseatbeatsURL-->', req.url.indexOf('https://api.cloudinary.com/v1_1/mpilopillz/image/upload'));
        const authToken = this.authService.getToken();
        if (req.url.indexOf('https://api.cloudinary.com/v1_1/mpilopillz/image/upload') !== 0) {
        const authRequest = req.clone({
            headers: req.headers.set('Authorization', 'Bearer ' + authToken)
        });
        return next.handle(authRequest);
    } else {
        const authRequest = req.clone({
        }); 
        return next.handle(authRequest);
    }
    }
}