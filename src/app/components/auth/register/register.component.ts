import { Component, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { PasswordValidator } from 'src/app/validators/validators';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  isLoading = false;
  regError: string;
  netWorkError: string;

  constructor(private authService: AuthService, private router: Router, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.initForm();
  }

  private initForm() {
    this.registerForm = this.formBuilder.group({
      rEmail: ['', [Validators.required, Validators.email]],
      rPassword: ['', [Validators.required, Validators.minLength(6)]],
      rConfirmPassword: ['', Validators.required]
  }, {
    validators: [ PasswordValidator ]
  });
  }

  onRegister() {
    if (!this.registerForm.valid) {
          return;
        }
    this.isLoading = true;
    this.authService.createUser(this.registerForm.value.rEmail, this.registerForm.value.rPassword)
    .subscribe({
      next: response => {
        console.log(response);
        this.authService.newIsAuthenticated.next(true);
        this.authService.showRegSuccessNotification.next(true);
        this.router.navigateByUrl('/login');
        setTimeout(() => {
            this.authService.showRegSuccessNotification.next(false);
        }, 5000);
    },
    error: err => {
      if (!err.status) {
        this.registerForm.setErrors({ noConnection: true});
        this.regError = 'No internet conecction was detected. Registration unsuccessful.';
        // this.netWorkError = 'No internet conecction was detected. Registration unsuccessful.';
      } else if(err.status === 500) {
        this.regError = 'Username already registerd';
        this.isLoading = false;
      } else {
        this.registerForm.setErrors({ unknownError: true});
        this.regError = 'Reg Unsuccessful. Please caontact support.';
        console.log(err);
        this.isLoading = false;
      }
    }
    });
  }
}
