import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  isLoading: boolean = false;
  loginError;
  showRegSuccessNotification;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.initForm();
    this.authService.showRegSuccessNotification.subscribe(showNotification => {
      this.showRegSuccessNotification = showNotification;
    });
    this.authService.loginError.subscribe(showLoginErr => {
      this.loginError = showLoginErr;
    });
    this.authService.isLoading.subscribe(showSpinner => {
      this.isLoading = showSpinner;
    })
  }

  private initForm() {
    this.loginForm = new FormGroup({
      lEmail: new FormControl(null, {
        validators: [Validators.required, Validators.email]
      }),
      lPassword: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(5)]
      }),
    });
  }

  onLogin() {
    if(this.loginForm.invalid) {
      return;
    }
    // this.isLoading = true;
    this.authService.login(this.loginForm.value.lEmail, this.loginForm.value.lPassword);
    // this.isLoading = false;
  }

}