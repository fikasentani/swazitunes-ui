import { Component, OnInit } from '@angular/core';
import { Song } from 'src/app/models/song.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { SongsService } from 'src/app/services/songs.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { mimeType } from 'src/app/helpers/mime-type.validators';

@Component({
  selector: 'app-song-create',
  templateUrl: './song-create.component.html',
  styleUrls: ['./song-create.component.scss']
})
export class SongCreateComponent implements OnInit {
  enteredTitle = '';
  enteredArtist = '';
  song: Song;
  isLoading = false;
  form: FormGroup;
  imagePreview: string;
  mode = 'add';
  private songId: string;

  constructor(
    public songsService: SongsService,
    public route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      artist: new FormControl(null, { validators: [Validators.required] }),
      songUrl: new FormControl(null, { validators: [Validators.required] }),
      // image: new FormControl(null, {
      //   validators: [Validators.required],
      //   asyncValidators: [mimeType]
      // })
    });
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('songId')) {
        this.mode = 'edit';
        this.songId = paramMap.get('songId');
        this.isLoading = true;
        this.songsService.getSong(this.songId).subscribe(songData => {
          this.isLoading = false;
          this.song = {
            id: songData._id,
            title: songData.title,
            artist: songData.artist,
            songPath: songData.songPath,
            creator: songData.creator
          };
          this.form.setValue({
            title: this.song.title,
            artist: this.song.artist,
            song: this.song.songPath
          });
        });
      } else {
        this.mode = 'add';
        this.songId = null;
      }
    });
  }
  onSongPicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    console.log('file-->', file);
    
    this.form.patchValue({ song: file });
    // this.form.get('image').updateValueAndValidity();
    // const reader = new FileReader();
    // reader.onload = () => {
    //   this.imagePreview = reader.result as string;
    // };
    // reader.readAsDataURL(file);
  }

  onSaveSong() {
    if (this.form.invalid) {
      return;
    }
    this.isLoading = true;
    if (this.mode === 'add') {
      this.songsService.addSong(
        this.form.value.title,
        this.form.value.artist,
        this.form.value.image
      );
    } else {
      this.songsService.updateSong(
        this.songId,
        this.form.value.title,
        this.form.value.artist,
        this.form.value.image
      );
    }
    this.form.reset();
  }
}
