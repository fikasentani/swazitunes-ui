import { Component, OnInit, OnDestroy } from '@angular/core';
import { Song } from 'src/app/models/song.model';
import { Subscription } from 'rxjs';
import { AuthService } from '../../auth/auth.service';
import { SongsService } from 'src/app/services/songs.service';

@Component({
  selector: 'app-song-list',
  templateUrl: './song-list.component.html',
  styleUrls: ['./song-list.component.scss']
})
export class SongListComponent implements OnInit, OnDestroy {
  songs: Song[] = [];
  isLoading = false;
  totalSongs = 0;
  songsPerPage = 5;
  currentPage = 1;
  pageSizeOptions = [1, 2, 5, 10];
  userIsAuthenticated = false;
  userId: string;
  private songsSub: Subscription;
  private authStatusSub: Subscription;

  constructor(
    public songsService: SongsService,
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.songsService.getSongs(this.songsPerPage, this.currentPage);
    this.userId = this.authService.getUserId();
    this.songsSub = this.songsService
    .getSongUpdateListener()
    .subscribe((songData: { songs: Song[]; songCount: number }) => {
      this.isLoading = false;
      this.totalSongs = songData.songCount;
      this.songs = songData.songs;
    });
    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authStatusSub = this.authService
    .getAuthStatusListener()
    .subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
      this.userId = this.authService.getUserId();
    });
  }

  onChangedPage(pageData ) {
    this.isLoading = true;
    this.currentPage = pageData.pageIndex + 1;
    this.songsPerPage = pageData.pageSize;
    this.songsService.getSongs(this.songsPerPage, this.currentPage);
  }

  onDelete(songId: string) {
    this.isLoading = true;
    this.songsService.deleteSong(songId).subscribe(() => {
      this.songsService.getSongs(this.songsPerPage, this.currentPage);
    });
  }

  ngOnDestroy() {
    this.songsSub.unsubscribe();
    this.authStatusSub.unsubscribe();
  }
}
