import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserProfileService } from 'src/app/services/user-profile.service';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { mimeType } from '../../../helpers/mime-type.validators';
import { UserProfile } from 'src/app/models/user-profile.model';
import { ThrowStmt } from '@angular/compiler';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-update-user-profile',
  templateUrl: './update-user-profile.component.html',
  styleUrls: ['./update-user-profile.component.scss']
})
export class UpdateUserProfileComponent implements OnInit {
  firstName = '';
  lastName = '';
  imagePath = '';
  userProfile: UserProfile;
  user;
  isLoading = false;
  userProfileForm: FormGroup;
  imagePreview: string;
  public mode = 'create';
  private userId: string;

  constructor(
    private userProfileService: UserProfileService,
    private route: ActivatedRoute,
    private authService: AuthService ,
    private router: Router) { }

  ngOnInit(): void {
    console.log('auth-->', this.authService.getToken());
    this.initForm();

    this.route.paramMap.subscribe((paramMap: ParamMap) => {
      if (paramMap.has('userId')) {
        this.mode = 'edit';
        this.userId = paramMap.get('userId');
        this.isLoading = true;
        this.authService.getUserInfo(this.userId).subscribe(userData => {
          this.isLoading = false;
          this.user = {
            id: userData._id,
            firstName: userData.firstName,
            lastName: userData.lastName,
            uImage: this.user.image
          };
          this.userProfileForm.setValue({
            uFirstName: this.user.firstName,
            uLastName: this.user.lastName,
            uImage: this.user.image
            // uImagePath: this.user.imagePath
          });
        });
      } else {
        this.mode = 'create';
        this.userId = null;
      }
    });
    this.userProfileForm.reset();
  }

  private initForm() {
    this.userProfileForm = new FormGroup({
      uFirstName: new FormControl(this.firstName, {
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(10)]
      }),
      uLastName: new FormControl(this.lastName, {
        validators: [Validators.required, Validators.minLength(3), Validators.maxLength(10)]
      }),
      uImage: new FormControl(null, {validators: [Validators.required], asyncValidators: [mimeType]})
      // uImagePath: new FormControl(this.imagePath, {

      // }),

    });
  }

  onUpdateProfile() {
    if (!this.userProfileForm.valid) {
          return;
        }
    this.isLoading = true;
    this.userProfileService.updateUserInfo(
          this.userId,
          this.userProfileForm.value.uFirstName,
          this.userProfileForm.value.uLastName,
          this.userProfileForm.value.uImage
        );
    this.isLoading = false;
    this.userProfileForm.reset();
  }


  onImagePicked(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    this.userProfileForm.patchValue({uImage: file});
    this.userProfileForm.get('uImage').updateValueAndValidity();
    const reader = new FileReader();
    reader.onload = () => {
      this.imagePreview = reader.result as string;
    };
    reader.readAsDataURL(file);
  }
}


// ngOnInit(): void {
//   console.log('auth-->',this.authService.getToken());
//   this.initForm();
//   // this.firstName = this.userProfileService.firstName;
//   // this.lastName = this.userProfileService.lastName;

//   // console.log('fn-->', this.firstName);


//   this.route.paramMap.subscribe((paramMap: ParamMap) => {
//     if (paramMap.has('userId')) {
//       this.mode = 'edit';
//       this.userId = paramMap.get('userId');
//       this.isLoading = true;
//       this.userProfileService.getUser(this.userId).subscribe(userData => {
//         this.isLoading = false;
//         this.userProfile = {
//           id: userData._id,
//           firstName: userData.firstName,
//           lastName: userData.lastName,
//           imagePath: userData.imagePath,
//           creator: userData.creator
//         };
//         this.userProfileForm.setValue({
//           uFirstName: this.userProfile.firstName,
//           uLastName: this.userProfile.lastName,
//           uImage: this.userProfile.imagePath
//         });
//       });
//     } else {
//       this.mode = 'create';
//       this.userId = null;
//     }
//   });
//   this.userProfileForm.reset();
// }
