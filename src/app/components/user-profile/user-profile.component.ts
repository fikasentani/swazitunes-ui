import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { UserProfileService } from 'src/app/services/user-profile.service';
import { UserProfile } from 'src/app/models/user-profile.model';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';
import { ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss']
})
export class UserProfileComponent implements OnInit {
  firstName;
  lastName;
  image;
  userId;
  userEyeDee: string;
  isLoading = false;
  userInfo: UserProfile[] = [];
  private userInfoSub: Subscription;

  userProfileInfo: UserProfile[] = [];
  constructor(
    private userProfileService: UserProfileService,
    private authService: AuthService,
    private route: ActivatedRoute) { }

 

  ngOnInit(): void {
    this.isLoading = true;
    this.route.paramMap.subscribe((paramMap: ParamMap) => {
     this.userId = paramMap.get('userId');
     console.log(paramMap);
     this.authService.getUserInfo(this.userId).subscribe(user => {
      this.isLoading = false;
      this.firstName = user.firstName;
      this.lastName = user.lastName;
      this.image = user.imagePath;
    });
   });
  }

  ngOnDestroy() {
    // this.userInfoSub.unsubscribe();
  }
}


// ngOnInit(): void {
//   this.isLoading = true;
//   this.userProfileService.getUserProfileData();
//   this.userEyeDee = this.authService.getUserId();
//   this.userInfoSub = this.userProfileService.getUserInfoUpdateListener()
//   .subscribe((user: UserProfile[]) => {
//     this.isLoading = false;
//     this.userInfo = user;
//     user.forEach(info => {
//       console.log("info", info);
      
//       this.firstName = info.firstName;
//       this.lastName = info.lastName;
//       this.userId = info.id;
//       this.image = info.imagePath;
//     })
    
//   });
// }


 // ngOnInit(): void {
  //   this.isLoading = true;
  //   this.userProfileService.getUserProfile();
  //   // this.userProfileService.getUserProfileData();
  //   // this.userEyeDee = this.authService.getUserId();
  //   this.userInfoSub = this.userProfileService.getUserInfoUpdateListener()
  //   .subscribe((user: UserProfile[]) => {
  //     this.isLoading = false;
  //     this.userInfo = user;
  //     user.forEach(info => {
  //       console.log("info", info);
        
  //       this.firstName = info.firstName;
  //       this.lastName = info.lastName;
  //       // this.userId = info.id;
  //       this.image = info.imagePath;
  //     })
      
  //   });
  // }
