import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ServerSideRenderingService } from 'src/app/services/server-side-rendering.service';

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.scss']
})
export class AudioComponent implements OnInit {
  content;
  @ViewChild('ejsContainer', {static: true}) ejsContainer: ElementRef
  constructor(private serverSideRenderingService: ServerSideRenderingService) { }

  ngOnInit(): void {
  }

  onViewCampGround() {
    this.serverSideRenderingService.getFromServer().subscribe(response => {
      this.content = response
      console.log('mpilo-->',this.content);
      this.ejsContainer.nativeElement.innerHTML = this.content;
  });
    
  }

}
