import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  userIsAuthenticated = false;
  userId = localStorage.getItem('userId');
  private authListenerSubs: Subscription;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit(): void {
    // this.userIsAuthenticated = this.authService.getIsAuth();
    // this.authListenerSubs = this.authService
    // .getAuthStatusListener()
    // .subscribe(isAuthenticated => {
    //   this.userIsAuthenticated = isAuthenticated;
    // });
    this.authService.newIsAuthenticated.subscribe(isAuth => this.userIsAuthenticated = isAuth);

  }

  goToHome() {
    this.router.navigateByUrl('');
  }

  goToProfile() {
    console.log('SOORY-->', this.userId);

    // this.router.navigate(['user-profile', this.userId])
    // THIS DOES NOT NEED TO CALL THE API CALL LOCAL STORAGE INSTEAD
    // this.authService.getUserInfo(this.userId).subscribe(user => {

    // })
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }
}
