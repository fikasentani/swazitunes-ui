import { Component, OnInit, ViewChild, OnDestroy, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-hamburger-menu',
  templateUrl: './hamburger-menu.component.html',
  styleUrls: ['./hamburger-menu.component.scss']
})
export class HamburgerMenuComponent implements OnInit, OnDestroy {
  hideMenuItems = true;
  isAuth = false;
  userId = localStorage.getItem('userId');
  private authListenerSubs: Subscription;
  toggled = true;
  menuListItems = [
    {
    text: 'home',
    clickAction: () => {
      this.router.navigateByUrl('/');
    }
    },
    {
      text: 'music player',
      clickAction: () => {
        this.router.navigateByUrl('listen');
      }
    },
    {
      text: 'account',
      isAuthRequired: true,
      clickAction: () => {
        this.router.navigateByUrl('account');
      }
    },
    {
      text: 'Terms and Conditions',
      clickAction: () => {
        this.openTermsConditions();
      }
    },
    {
      text: 'Help',
      clickAction: () => {
        this.router.navigateByUrl('help');
      }
    },
    {
      text: 'Your Profile',
      isAuthRequired: true,
      clickAction: () => {
        this.router.navigate(['user-profile', this.userId]);
      }
    },
    {
      text: 'logout',
      isAuthRequired: true,
      clickAction: () => {
        this.authService.logout();
      }
    },
    {
      text: 'logIn',
      isAuthRequired: false,
      clickAction: () => {
        this.router.navigateByUrl('login');
      }
    },
    {
      text: 'Register',
      isAuthRequired: false,
      clickAction: () => {
        this.router.navigateByUrl('register');
      }
    }
  ];
  constructor(
    private router: Router,
    private authService: AuthService,
    private renderer: Renderer2
  ) { }

  ngOnInit(): void {
    this.isAuth = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
    .getAuthStatusListener()
    .subscribe(isAuthenticated => {
      this.isAuth = isAuthenticated;
    });
  }

  openTermsConditions() {
    console.log('outta space bih');
  }

  toggleHamburgerMenu() {
    this.toggled = !this.toggled;
    this.renderer[!this.toggled ? 'addClass': 'removeClass'](document.body, 'body--position__fixed')
  }

  ngOnDestroy() {
    this.authListenerSubs.unsubscribe();
  }

}
