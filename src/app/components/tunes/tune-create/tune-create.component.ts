import { Component, OnInit } from '@angular/core';
import { Tune } from 'src/app/models/tune.model';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { TunesService } from 'src/app/services/tunes.service';

@Component({
  selector: 'app-tune-create',
  templateUrl: './tune-create.component.html',
  styleUrls: ['./tune-create.component.scss']
})
export class TuneCreateComponent implements OnInit {
  title: string;
    artist: string;
    imagePath: string;
    creator: string;

    tune: Tune;
    isLoading = false;
    form: FormGroup;
    mode = 'add';
    private tuneId: string;

  constructor(public route: ActivatedRoute, public tunesService: TunesService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.form = new FormGroup({
      title: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(2)],
      }),
      artist: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(2)]
      }),
      imagePath: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1)]
      }),
      tunePath: new FormControl(null, {
        validators: [Validators.required, Validators.minLength(1)]
      }),
  });


    this.route.paramMap.subscribe((paramMap: ParamMap) => {
    if (paramMap.has('tuneId')) {
      this.mode = 'edit';
      this.tuneId = paramMap.get('tuneId');
      this.isLoading = true;
      this.tunesService.getTune(this.tuneId).subscribe(tuneData => {
        this.isLoading = false;
        this.tune = {
              id: tuneData._id,
              title: tuneData.title,
              artist: tuneData.artist,
              imagePath: tuneData.imagePath,
              tunePath: tuneData.tunePath,
              creator: tuneData.creator,
        };
        this.form.setValue({
              title: this.tune.title,
              artist: this.tune.artist,
              imagePath: this.tune.imagePath,
              tunePath: this.tune.tunePath,
        });
      });
    }
  });
}

onSaveTune() {
  if (this.form.invalid) {
    return;
  }
  console.log('-->clicked');
  
  this.isLoading = true;
  if (this.mode === 'add') {
    this.tunesService.addTune(
              this.form.value.title,
              this.form.value.artist,
              this.form.value.imagePath,
              this.form.value.tunePath,
    );
  } else {
    this.tunesService.updateTune(
              this.tuneId,
              this.form.value.title,
              this.form.value.artist,
              this.form.value.imagePath,
              this.form.value.tunePath,
    );
  }
}
}
