import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Tune } from 'src/app/models/tune.model';
import { TunesService } from 'src/app/services/tunes.service';
import { AuthService } from '../../auth/auth.service';

@Component({
  selector: 'app-tune-list',
  templateUrl: './tune-list.component.html',
  styleUrls: ['./tune-list.component.scss']
})
export class TuneListComponent implements OnInit {
  userIsAuthenticated = false;
  private authListenerSubs: Subscription;
  tunes: Tune[] = [];
  isLoading = false;
  
  private tunesSub: Subscription;

  constructor(private tunesService: TunesService, private authService: AuthService) { }

  ngOnInit(): void {
    this.isLoading = true;
    this.tunesService.getTunes();
    this.tunesSub = this.tunesService.getTunesUpdateListener()
      .subscribe((tunes: Tune[]) => {
        this.isLoading = false;
        this.tunes = tunes;
        console.log('tel-->', tunes);
      });

    this.userIsAuthenticated = this.authService.getIsAuth();
    this.authListenerSubs = this.authService
    .getAuthStatusListener()
    .subscribe(isAuthenticated => {
      this.userIsAuthenticated = isAuthenticated;
    });
  }

  onDelete(tuneId: string) {
    this.tunesService.deleteTune(tuneId);
  }

}
