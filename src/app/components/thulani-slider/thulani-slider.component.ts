import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-thulani-slider',
  templateUrl: './thulani-slider.component.html',
  styleUrls: ['./thulani-slider.component.scss']
})
export class ThulaniSliderComponent implements OnInit {
  slideIndex: number = 1;
  constructor(private renderer: Renderer2) { }

  ngOnInit(): void {
    this.addFunctionalityToSlide();
    // setInterval(() => {
    //   this.plusSlides(-1)
    // }, 3000)
  }

  addFunctionalityToSlide() {
    this.showSlides(this.slideIndex);
  }

  showSlides(n) {
    let i;
    let slides = document.querySelectorAll(".mySlides");
    let dots = document.querySelectorAll(".dot");
   
    if (n > slides.length) {
       this.slideIndex = 1;
     }
    if (n < 1) {
       this.slideIndex = slides.length
     }

    slides.forEach(slide => {
       this.renderer.setStyle(slide, 'display', 'none');
     });

    dots.forEach(dot => {
      dot.className = dot.className.replace(" active", "");
     });
    this.renderer.setStyle(slides[this.slideIndex - 1], 'display', 'block');
    dots[this.slideIndex - 1].className += " active";

  }

  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }


}
