import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {ReactiveFormsModule, FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { UserProfileComponent } from './components/user-profile/user-profile.component';
import { AudioComponent } from './components/server-side/audio/audio/audio.component';
import { UpdateUserProfileComponent } from './components/user-profile/update-user-profile/update-user-profile.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { LoadingSpinnerComponent } from './components/loading-spinner/loading-spinner.component';
import { HamburgerMenuComponent } from './components/hamburger-menu/hamburger-menu.component';
import { ThulaniSliderComponent } from './components/thulani-slider/thulani-slider.component';
import { FooterComponent } from './components/footer/footer.component';
import { AuthInterceptor } from './components/auth/auth-interceptor';
import { SongListComponent } from './components/songs/song-list/song-list.component';
import { SongCreateComponent } from './components/songs/song-create/song-create.component';
import { TuneCreateComponent } from './components/tunes/tune-create/tune-create.component';
import { TuneListComponent } from './components/tunes/tune-list/tune-list.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    UserProfileComponent,
    AudioComponent,
    UpdateUserProfileComponent,
    HomeComponent,
    HeaderComponent,
    LoadingSpinnerComponent,
    HamburgerMenuComponent,
    ThulaniSliderComponent,
    FooterComponent,
    SongListComponent,
    SongCreateComponent,
    TuneCreateComponent,
    TuneListComponent,
  ],
  imports: [
    ReactiveFormsModule,
    FormsModule,
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
