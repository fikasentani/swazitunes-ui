import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { UserProfile } from '../models/user-profile.model';
import { environment } from 'src/environments/environment';
// import { userInfo } from 'os';

@Injectable({
    providedIn: 'root'
})
export class UserProfileService {
    userInfo: UserProfile[] = [];
    private userInfoUpdated = new Subject<UserProfile[]>();
    firstName = '';
    lastName = '';
    private apiUrl = environment.baseApiUrl;
    constructor(private http: HttpClient, private router: Router) {}

    getUserProfile() {
        this.http.get<{message: string; userProfile: any}>(`${this.apiUrl}/api/auth/profile`)
        .pipe(
            map(userData => {
                return userData.userProfile.map(user => {
                    return {
                        firstName: user.firstName,
                        lastName: user.lastName,
                        imagePath: user.imagePath,
                    };
                });
            })
        ).subscribe(transformedUserInfo => {
            this.userInfo = transformedUserInfo;
            this.userInfoUpdated.next([...this.userInfo]);
            this.userInfo.map(info => {
                this.firstName = info.firstName;
                this.lastName = info.lastName;
            });
        });
    }
    getUserProfileData() {
        this.http
        .get<{message: string; userProfile: any}>(`${this.apiUrl}/api/user-profile/user-profile`)
        .pipe(
            map(userData => {
                return userData.userProfile.map(user => {
                    return {
                        firstName: user.firstName,
                        lastName: user.lastName,
                        id: user._id,
                        imagePath: user.imagePath,
                        creator: user.creator
                    };
                });
            })
        ).subscribe(transformedUserInfo => {
            this.userInfo = transformedUserInfo;
            this.userInfoUpdated.next([...this.userInfo]);
            this.userInfo.map(info => {
                this.firstName = info.firstName;
                this.lastName = info.lastName;
            });
        });
    }

    getUserInfoUpdateListener() {
        return this.userInfoUpdated.asObservable();
    }

    getUser(id: string) {
        return this.http.get<{_id: string, firstName: string, lastName: string, imagePath: string, creator: string}>(
            `${this.apiUrl}/api/user/user/${id}`
        );
    }

    private uploadImageToCloudinary(imagePath, firstName) {
        const imageData = new FormData();
        imageData.append('file', imagePath, firstName);
        imageData.set('upload_preset', 'ml_default');
        imageData.set(
          'public_id',
          `swazitunes/${firstName}${new Date().getTime()}`
        );
        return this.http.post<{ url: string }>(
          `https://api.cloudinary.com/v1_1/mpilopillz/image/upload`,
          imageData
        );
      }
    
    addUserInfo(firstName: string, lastName: string, image: File) {
        // this.uploadImageToCloudinary()
        const userData = new FormData();
        userData.append('firstName', firstName);
        userData.append('lastName', lastName);
        userData.append('image', image, firstName);
        this.http
        .post<{ message: string; userInfo: UserProfile}>(
            `${this.apiUrl}/api/user-profile/user-profile`,
            userData
        ).subscribe(responseData => {
            const user: UserProfile = {
                id: responseData.userInfo.id,
                firstName,
                lastName,
                imagePath: responseData.userInfo.imagePath,
                creator: null
            };
            this.userInfo.push(user);
            this.userInfoUpdated.next([...this.userInfo]);
            this.router.navigate(['/user-profile']);
        });
    }

    updateUserInfo(id: string, firstName: string, lastName: string, image: File | string) {
        this.uploadImageToCloudinary(image, firstName)
            .pipe(
                map(({ url }) => ({
                    id,
                    firstName,
                    lastName,
                    imagePath: url
                })),
                switchMap((user) =>  this.http
                .patch(`${this.apiUrl}/api/auth/${id}/update-profile`, user))
            ).subscribe((user) => {
                this.router.navigate(['/user-profile']);
            });
        
        
        //     let userData: UserProfile | FormData;
        // if (typeof image === 'object') {
        //     userData = new FormData();
        //     userData.append('id', id);
        //     userData.append('firstName', firstName);
        //     userData.append('lastName', lastName);
        //     userData.append('image', image,  firstName);
        // } else {
        //     userData = {
        //         id,
        //         firstName,
        //         lastName,
        //         imagePath: image,
        //         creator: null
        //     };
        // }
        // this.http
        // .put(`${this.apiUrl}/api/user-profile/user-profile/${id}`, userData)
        // .subscribe(response => {
        //     this.router.navigate(['/user-profile']);
        // });
    }
}
