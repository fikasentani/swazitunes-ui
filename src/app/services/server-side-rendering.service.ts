import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ServerSideRenderingService {
    constructor(private http: HttpClient) {}
   
    getFromServer(){
       return this.http.get('http://localhost:3000/campgrounds', {responseType: 'text'})
    }

    getTermsAndConds() {
        console.log('wo wo');
        
    }
}
