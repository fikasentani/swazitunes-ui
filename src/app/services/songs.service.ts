import { Song } from '../models/song.model';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { stringify } from 'querystring';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { environment } from 'src/environments/environment';
import { map, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class SongsService {
    private apiUrl = environment.baseApiUrl;
    private songs: Song[] = [];
    private songsUpdated = new Subject<{ songs: Song[]; songCount: number}>();

    constructor(private http: HttpClient, private router: Router) {}

    getSongs(songsPerPage: number, currentPage: number) {
        const queryParams = `?pagesize=${songsPerPage}&page=${currentPage}`;
        this.http
        .get<{ message: string; songs: any; maxSongs: number}>(
            `${this.apiUrl}/api/songs/${queryParams}`
        )
        .pipe(
            map(songData => {
                return {
                    songs: songData.songs.map(song => {
                        return {
                            title: song.title,
                            artist: song.artist,
                            id: song._id,
                            songPath: song.songPath,
                            creator: song.creator
                        };
                    }),
                    maxSongs: songData.maxSongs
                };
            })
        )
        .subscribe(transformedSongData => {
            this.songs = transformedSongData.songs;
            this.songsUpdated.next({
                songs: [...this.songs],
                songCount: transformedSongData.maxSongs
            });
        });
    }

    getSongUpdateListener() {
        return this.songsUpdated.asObservable();
    }

    getSong(id: string) {
        return this.http.get<{
            _id: string;
            title: string;
            artist: string;
            songPath: string;
            creator: string;
        }>(`${this.apiUrl}/api/songs/${id}`);
    }

    addSong(title: string, artist: string, song: File | string) {
        const songData = new FormData();
        songData.append('title', title);
        songData.append('artist', artist);
        songData.append('song', song, title);
        this.http
        .post<{ message: string; song: Song }>(
            `${this.apiUrl}/api/songs`,
            songData
        )
        .subscribe(responseData => {
            this.router.navigate(['/']);
        });
    }

    private uploadSongToCloudinary(songPath, firstName) {
        const songData = new FormData();
        songData.append('file', songPath, firstName);
        songData.set('upload_preset', 'ml_default');
        songData.set(
          'public_id',
          `swazitunes/${firstName}${new Date().getTime()}`
        );
        return this.http.post<{ url: string }>(
          `https://api.cloudinary.com/v1_1/mpilopillz/image/upload`,
          songData
        );
      }

      updateSong(id: string, title: string, artist: string, song: File | string) {
        this.uploadSongToCloudinary(song, title)
            .pipe(
                map(({ url }) => ({
                    id,
                    title,
                    artist,
                    songPath: url
                })),
                switchMap((user) =>  this.http
                .patch(`${this.apiUrl}/api/songs/${id}/update-song`, user))
            ).subscribe((user) => {
                this.router.navigate(['/user-profile']);
            });
    }

    updateSongOldSchool(id: string, title: string, artist: string, song: File | string) {
        let songData: Song | FormData;
        if (typeof song === 'object') {
            songData = new FormData();
            songData.append('id', id);
            songData.append('title', title);
            songData.append('artist', artist);
            songData.append('song', song, title);
        } else {
            songData = {
                id,
                title,
                artist,
                songPath: song,
                creator: null
            };
        }
        this.http
        .put(`${this.apiUrl}/api/songs/${id}`, songData)
        .subscribe(response => {
            this.router.navigate(['/']);
        });
    }

    deleteSong(songId: string) {
        return this.http.delete(`${this.apiUrl}/api/songs/${songId}`);
    }
}