import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Tune } from '../models/tune.model';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root'})
export class TunesService {
    private tunes: Tune[];
    private tunesUpdated = new Subject<Tune[]>();
    private apiUrl = environment.baseApiUrl;

    constructor(private http: HttpClient, private router: Router) {}

    getTunes() {
        this.http.get<{message: string; tunes: any}>(`${this.apiUrl}/api/tunes`)
        .pipe(
            map(tunesData => {
                return tunesData.tunes.map(tune => {
                    return {
                        title: tune.title,
                        artist: tune.artist,
                        id: tune._id,
                        imagePath: tune.imagePath,
                        tunePath: tune.tunePath,
                        creator: tune.creator
                    };
                });
            })
        ). subscribe(transformedTunes => {
            this.tunes = transformedTunes;
            this.tunesUpdated.next([...this.tunes]);
        });
    }

    getTunesUpdateListener() {
        return this.tunesUpdated.asObservable();
    }

    getTune(id: string) {
        return this.http.get<{
            _id: string;
            title: string;
            artist: string;
            imagePath: string;
            tunePath: string;
            creator: string;
        }>(`${this.apiUrl}/api/tunes/${id}`);
    }

    addTune(title, artist, imagePath, tunePath) {
        console.log('title',title );
        console.log('artist',artist );
        console.log('image',imagePath );
        console.log('tune',tunePath );
        
        const tune: Tune = {
                id: null,
                title,
                artist,
                imagePath,
                tunePath,
                creator: null
        };
        this.http
        .post<{message: string; tuneId: string }>(
            `${this.apiUrl}/api/tunes`,
            tune
        ).subscribe(responseData => {
            const id = responseData.tuneId;
            tune.id = id;
            this.tunes.push(tune);
            this.tunesUpdated.next([...this.tunes]);
            this.router.navigate(['/']);
        });
    }
    
    updateTune(id, title, artist, imagePath, tunePath) {
        const tune: Tune = {
                id,
                title,
                artist,
                imagePath,
                tunePath,
                creator: null
        };
        this.http
        .patch(`${this.apiUrl}/api/tunes/${id}`, tune)
        .subscribe(response => {
            const updatedTunes = [...this.tunes];
            const oldTuneIndex = updatedTunes.findIndex(t => t.id === id);
            updatedTunes[oldTuneIndex] = tune;
            this.tunes = updatedTunes;
            this.tunesUpdated.next([...this.tunes]);
            this.router.navigate(['/']);
        })
    }

    deleteTune(tuneId: string) {
        this.http
          .delete(`${this.apiUrl}/api/tunes/${tuneId}`)
          .subscribe(() => {
            const updatedTunes = this.tunes.filter(tune => tune.id !== tuneId);
            this.tunes = updatedTunes;
            this.tunesUpdated.next([...this.tunes]);
          });
      }
}
